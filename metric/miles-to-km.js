
// This snippet replaces 'miles' with 'km' on nationbuilder 
// event distance search dialogs.  It does *not* change the
// search distance, which may also be possible -- but is not tested
// let us know if you try it!

  // select the radio button labels inside div.distance
  $('div.distance label.radio').each(function() {
    // replace 'miles' with 'km' for each label
    $(this).html($(this).html().replace(/miles?/,'km'));
  });